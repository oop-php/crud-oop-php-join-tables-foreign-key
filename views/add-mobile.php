<?php
require('../db/ConnectionController.php');
// $conn = new ConnectionController();

include('../controllers/MobilesController.php');
$mobilni = new Mobiles;
$joinMobilni = $mobilni->displayJoinTables();

// var_dump($joinMobilni);


if(isset($_POST['submit'])){
    $createMobilni = $mobilni->addMobile($_POST);
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Mobilni</title>
    <link rel="stylesheet" href="../css/main.css">
</head>

<body>

    <div class="container">
        <h1>Dodaj mobilni telefoni</h1>
        <form method="POST" action="">
            <div class="mb-3">
                <label for="proizvodjac_id" class="form-label">Proizvodjac</label>

                <select class="form-control" name="proizvodjac_id" id="proizvodjac_id">
                    <option value="0">Odaberi Proizvodjaca</option>
                    <?php
                    if (mysqli_num_rows($joinMobilni) > 0) {
                        $row = mysqli_fetch_array($joinMobilni);

                        while ($row = mysqli_fetch_array($joinMobilni)) {
                    ?>
                            <option value="<?php echo $row['id_pr'];?>"><?php echo $row['ime_proizvodjaca']; ?></option>
                    <?php
                        }
                    }

                    ?>
                </select>

            </div>
            <div class="mb-3">
                <label for="model_telefona" class="form-label">Model Telefona</label>
                <input type="text" class="form-control" id="model_telefona" name="model_telefona" value="">
            </div>
            <div class="mb-3">
                <label for="cena" class="form-label">Cena</label>
                <input type="text" class="form-control" id="cena" aria-describedby="cena" name="cena" value="">
            </div>
            <div class="mb-3">
                <label for="godina_proizvodnje" class="form-label">Godina Proizvodnje</label>
                <input type="date" class="form-control" id="godina_proizvodnje" name="godina_proizvodnje" value="">
            </div>
            <div class="mb-3">
                <label for="vreme_unosa" class="form-label">Vreme Unosa</label>
                <input type="datetime-local" class="form-control" id="vreme_unosa" name="vreme_unosa" value="">
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Unesi novi mobilni telefon</button>
            <a href="../index.php" class="btn btn-success">Vrati se na listu telefona</a>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>