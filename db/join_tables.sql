-- inner join --
SELECT proizvodjac_id
FROM mobilni_telefoni
LEFT JOIN proizvodjaci
ON mobilni_telefoni.proizvodjac_id = proizvodjaci.id;

SELECT proizvodjac_id FROM mobilni_telefoni LEFT JOIN proizvodjaci ON mobilni_telefoni.proizvodjac_id = proizvodjaci.id_pr;

--- foreign key ---

ALTER TABLE `mobilni_telefoni` ADD CONSTRAINT `proizvodjac_fk` FOREIGN KEY (`proizvodjac_id`) REFERENCES `proizvodjaci`(`id_pr`) ON DELETE RESTRICT ON UPDATE RESTRICT;


