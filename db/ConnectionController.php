<?php

class ConnectionController
{
    public $conn;

    public function __construct()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "mobilni_uredjaji";

        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }else{
            echo 'Connected';
        }
        // var_dump($this->conn = $conn);
        $this->conn = $conn;
    }

    public function query($query)
    {
        // $this->conn->execute_query($query);
        $dbres=mysqli_query($this->conn, $query);
        // var_dump($dbres);
        return $dbres;
    }
}

/*
Upotrebljava se ovako:

$conn = new ConnectionController();
*/
