-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2023 at 02:48 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobilni_uredjaji`
--

-- --------------------------------------------------------

--
-- Table structure for table `mobilni_telefoni`
--

CREATE TABLE `mobilni_telefoni` (
  `id` int(11) NOT NULL,
  `proizvodjac_id` int(11) NOT NULL,
  `model_telefona` varchar(50) NOT NULL,
  `cena` decimal(8,2) NOT NULL,
  `godina_proizvodnje` date NOT NULL,
  `vreme_unosa` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mobilni_telefoni`
--

INSERT INTO `mobilni_telefoni` (`id`, `proizvodjac_id`, `model_telefona`, `cena`, `godina_proizvodnje`, `vreme_unosa`) VALUES
(1, 3, 'A1', 1000.00, '2023-11-29', '2023-11-21 16:16:40'),
(2, 1, 'F10', 2000.00, '2020-11-11', '2023-11-21 16:16:40'),
(3, 2, 'Alfa 7', 1000.00, '2022-11-03', '2023-11-22 11:43:33'),
(4, 4, 'Type 10', 2000.00, '2022-11-10', '2023-11-22 11:43:33'),
(5, 3, 'top512', 600.00, '2022-11-02', '2023-11-22 11:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `proizvodjaci`
--

CREATE TABLE `proizvodjaci` (
  `id` int(11) NOT NULL,
  `ime_proizvodjaca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `proizvodjaci`
--

INSERT INTO `proizvodjaci` (`id`, `ime_proizvodjaca`) VALUES
(1, 'Nokia'),
(2, 'Siemens'),
(3, 'Huawei'),
(4, 'Iphone');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mobilni_telefoni`
--
ALTER TABLE `mobilni_telefoni`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `proizvodjac_fk` (`proizvodjac_id`);

--
-- Indexes for table `proizvodjaci`
--
ALTER TABLE `proizvodjaci`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mobilni_telefoni`
--
ALTER TABLE `mobilni_telefoni`
  ADD CONSTRAINT `proizvodjac_fk` FOREIGN KEY (`proizvodjac_id`) REFERENCES `proizvodjaci` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
