<?php
require('./db/ConnectionController.php');
// $conn = new ConnectionController();
/*
//RAD SA MODELOM PREKO BAZE
include('./models/MobilePhone.php');
$proizvodjac = new MobilePhone();
$proizvodjac->getProizvodjac(2);
*/

include('./controllers/MobilesController.php');
$mobilni = new Mobiles;
$joinMobilni = $mobilni->displayJoinTables();
// var_dump($joinMobilni);


?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Mobilni</title>
    <link rel="stylesheet" href="./css/main.css">
</head>

<body>

    <div class="container">
        <h1>CRUD mobilni telefoni</h1>


        <div class="mt-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Mobilni telefoni pregled</h4>
                            <a href="./views/add-mobile.php" type="button" class="btn btn-success">Unesi podatke</a>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Proizvodjac</th>
                                    <th>Model Telefona</th>
                                    <th>Cena</th>
                                    <th>Godina Proizvodnje</th>
                                    <th>Vreme unosa</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                ?>
                                <?php
                                if (mysqli_num_rows($joinMobilni) > 0) {
                                    while ($row = mysqli_fetch_array($joinMobilni)) {
                                ?>
                                        <tr>

                                            <th scope="row"><?php echo $i++; ?></th>
                                            <td><?php echo $row['ime_proizvodjaca']; ?></td><!-- ovde pozivamo podatke iz druge tabele `proizvodjaci` ime proizvodjaca iz te tabele, preko join tabela i foreign key-a u tabeli `mobilni_telefoni` a foreign key je `proizvodjac_id`, metod displayJoinTables() pozvan iz instance na objekat $mobilni -->
                                            <td><?php echo $row['model_telefona']; ?></td>
                                            <td><?php echo $row['cena']; ?></td>
                                            <td><?php echo $row['godina_proizvodnje']; ?></td>
                                            <td><?php echo $row['vreme_unosa']; ?></td>
                                            <td>
                                                <a href="./views/mobile-edit.php" type="button" class="btn btn-primary">Izmeni podatke</a>
                                            </td>
                                            <td>
                                                <a href="./views/mobile-delete.php?id=<?= $res['id']; ?>" type="button" class="btn btn-danger">Obrisi telefon</a>
                                            </td>
                                        </tr>
                                <?php
                                    }
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>

</html>